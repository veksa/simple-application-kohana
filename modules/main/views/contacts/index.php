<div class="site-contact">
	<h1>Контакты</h1>

	<p>
		Для связи с нами используйте форму ниже.
	</p>

	<div class="row">
		<div class="col-lg-5">
			<?= Form::open( Route::url( 'default', [ 'controller' => 'contacts' ] ), [ 'id' => 'contact-form' ] ) ?>
			<div class="form-group field-contactform-name required">
				<?= Form::label( 'contactform-name', 'Ваше имя', [ 'class' => 'control-label' ] ) ?>
				<?= Form::input( 'ContactForm[name]', /*$validator['name']*/
					'', [ 'id' => 'contactform-name' ] ) ?>

				<p class="help-block help-block-error"></p>
			</div>
			<div class="form-group field-contactform-email required">
				<?= Form::label( 'contactform-email', 'Ваш e-mail', [ 'class' => 'control-label' ] ) ?>
				<?= Form::input( 'ContactForm[email]', /*$validator['email']*/
					'', [ 'id' => 'contactform-email' ] ) ?>

				<p class="help-block help-block-error"></p>
			</div>
			<div class="form-group field-contactform-body required">
				<?= Form::label( 'contactform-body', 'Ваше сообщение', [ 'class' => 'control-label' ] ) ?>
				<?= Form::textarea( 'ContactForm[body]', /*$validator['body']*/
					'', [
						'id' => 'contactform-body',
						'rows' => 6
					] ) ?>

				<p class="help-block help-block-error"></p>
			</div>
			<div class="form-group field-contactform-verifycode">
				<?= Form::label( 'contactform-verifycode', 'Код верификации', [ 'class' => 'control-label' ] ) ?>
				<div class="row">
					<div class="col-lg-3">
						<?=$captcha_image?>
					</div>
					<div class="col-lg-9">
						<?= Form::input( 'ContactForm[verifycode]', '', [ 'id' => 'contactform-verifycode' ] ) ?>
					</div>
				</div>

				<p class="help-block help-block-error"></p>
			</div>
			<?= Form::close() ?>

			<? /*
			<?= $form->field( $model, 'verifyCode' )
				->widget( Captcha::className(), [
					'captchaAction' => '/main/contacts/captcha',
					'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-9">{input}</div></div>',
				] ) ?>*/ ?>
			<div class="form-group">
				<?= Form::submit( 'contact-button', 'Отправить', [ 'class' => 'btn btn-primary' ] ) ?>
			</div>
		</div>
	</div>
</div>