<? defined( 'SYSPATH' ) or die( 'No direct script access.' );

class Controller_Default extends Controller_Layout_Default{
	public function action_index(){
		$this->template->layout = View::factory( 'default/index' );
	}
}
