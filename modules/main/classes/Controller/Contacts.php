<? defined( 'SYSPATH' ) or die( 'No direct script access.' );

class Controller_Contacts extends Controller_Layout_Default{
	public function action_index(){
		$this->template->title = 'Контакты';

		$this->breadcrumbs[Route::url( 'default', [ 'controller' => 'contacts' ] )] = $this->template->title;

		$captcha_image = Captcha::instance()
			->render();
		$this->template->layout = View::factory( 'contacts/index' )
			->bind( 'captcha_image', $captcha_image );;
	}
}
