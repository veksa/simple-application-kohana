<? defined( 'SYSPATH' ) or die( 'No direct script access.' ); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="<?= Kohana::$charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $title ?></title>
	<?
	echo html::style( 'vendor/twbs/bootstrap/dist/css/bootstrap.min.css' );
	echo html::style( 'css/site.css' );

	foreach( $meta_tags as $meta_tag ){
		if( is_array( $meta_tag ) ){
			echo html::meta( $meta_tag );
		}else{
			echo html::meta( $meta_tag['name'], $meta_tag['value'] );
		}
	}

	foreach( $links as $link ){
		echo html::link( $link['href'], $link['rel'], $link['type'], false );
	}

	foreach( $stylesheets as $stylesheet ){
		echo ( isset( $stylesheet['ie_only'] ) ) ? "<!--[if IE]>\n\t\t" : '';
		echo html::style( $stylesheet['href'], array( 'media' => $stylesheet['media'] ), false );
		echo ( isset( $stylesheet['ie_only'] ) ) ? "<![endif]-->\n\t\t" : '';
	}

	foreach( $javascripts as $javascript ){
		echo html::script( $javascript, NULL, false ) . "\t\t";
	}
	?>

	<? if( $js_custom ){ ?>
		<script type="text/javascript">
			<?= $js_custom ?>
		</script>
	<? } ?>
</head>
<body>
<div class="wrap">
	<nav id="w0" class="navbar-inverse navbar-fixed-top navbar" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#w0-collapse" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?= Kohana::$base_url ?>"><?= $site_name ?></a>
			</div>
			<div id="w0-collapse" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
				<ul id="w1" class="navbar-nav navbar-right nav">
					<li class="active"><a href="<?= Kohana::$base_url ?>">Главная</a></li>
					<li><a href="<?=Route::url( 'default', [ 'controller' => 'contacts' ] )?>">Контакты</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<?
	/*
	if( Yii::$app->user->isGuest ){
		$menuItems[] = [
			'label' => 'Регистрация',
			'url' => [ '/user/default/signup' ]
		];
		$menuItems[] = [
			'label' => 'Авторизация',
			'url' => [ '/user/default/login' ]
		];
	}else{
		$menuItems[] = [
			'label' => 'Проекты',
			'url' => [ '/projects/default/index' ]
		];
		$menuItems[] = [
			'label' => 'Выйти (' . Yii::$app->user->identity->username . ')',
			'url' => [ '/user/default/logout' ],
			'linkOptions' => [ 'data-method' => 'post' ]
		];
	}
	*/
	?>

	<div class="container">
		<?= Request::factory( 'breadcrumbs' )
			->query( [
				'template' => 'breadcrumbs/bootstrap',
				'items' => Breadcrumbs::get()
			] )
			->execute()
			->body() ?>
		<?/*= Alert::widget() */ ?>
		<?= isset( $layout ) ? $layout : '' ?>
	</div>
</div>

<footer class="footer">
	<div class="container">
		<p class="pull-left">&copy; <?= $site_name ?> <?= date( 'Y' ) ?></p>

		<p class="pull-right">Powered by <a href="https://kohanaframework.org/" rel="external">Kohana Framework</a></p>
	</div>
</footer>
</body>
</html>