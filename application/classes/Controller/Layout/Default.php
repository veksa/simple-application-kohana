<? defined( 'SYSPATH' ) or die( 'No direct script access.' );

abstract class Controller_Layout_Default extends Controller_Template{
	public $breadcrumbs;

	public $main_config;

	/**
	 * Auto loaded configs
	 *     Format:
	 *         array (group => params)
	 *
	 * @var array
	 */
	public $config = array();

	/**
	 * Default layout template
	 *
	 * @var View
	 */
	public $template = 'main';

	/**
	 * Before execute action
	 */
	public function before(){
		$this->main_config = Kohana::$config->load( 'main' );

		parent::before();

		// Page Title
		$this->template->title = $this->main_config->title;
		$this->template->site_name = $this->main_config->title;

		// Meta Tags
		$this->template->meta_tags = array();

		// Relational Links (other than stylesheets)
		$this->template->links = array();

		// Stylesheets
		$this->template->stylesheets = array();

		// Javascripts
		$this->template->javascripts = array();

		// Javascript Custom
		$this->template->js_custom = '';

		// Default layout
		$this->template->layout = new View( 'layouts/empty' );

		// No content by default
		$this->template->layout->content = '';

		// Layout Shortcut
		$this->layout = $this->template->layout;

		$this->breadcrumbs = [
			Kohana::$base_url => 'Главная'
		];
	}

	public function after(){
		Breadcrumbs::set( $this->breadcrumbs );

		parent::after();
	}
}